package mbe.td2015.projectqanda.common.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Session
 */
public class Session implements Serializable {
    private String name;
    private Date startingTime;
    private Speaker speaker;

    public Session(String name, Speaker speaker) {
        this.name = name;
        this.speaker = speaker;
        this.startingTime = new Date(System.currentTimeMillis());
    }

    public Session(String name, String computerName, Speaker speaker) {
        this.name = name;
        this.speaker = speaker;
        this.startingTime = new Date(System.currentTimeMillis());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartingTime(String format) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.parse(this.startingTime.toString());
    }

    public Speaker getSpeaker() {
        return speaker;
    }

    public void setSpeaker(Speaker speaker) {
        this.speaker = speaker;
    }
}
