package mbe.td2015.projectqanda.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

/**
 * Classe de sérialisation de Byte Array en Object et
 * vis-versa.
 */
public class ByteArraySerializer {

    /**
     * Sérialisation d'Object en Byte Array
     * @param obj
     * @return
     */
    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        byte[] data = null;

        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            data = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // ignorer l'exception
            }
            try {
                bos.close();
            } catch (IOException ex) {
                // ignorer l'exception
            }
        }
        return data;
    }

    /**
     * Sérialisation d'un Byte Array en Object
     * @param data
     * @return
     */
    public static Object deserialize(byte[] data) {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        ObjectInput in = null;
        Object obj = null;

        try {
            in = new ObjectInputStream(bis);
            obj = in.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bis.close();
            } catch (IOException ex) {
                // ignorer l'exception
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignorer l'exception
            }
        }
        return obj;
    }
}
