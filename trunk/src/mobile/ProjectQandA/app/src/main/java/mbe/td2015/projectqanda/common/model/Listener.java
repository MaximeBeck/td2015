package mbe.td2015.projectqanda.common.model;

import java.io.Serializable;

/**
 * Auditeur
 */
public class Listener extends User implements Serializable {
    public Listener() {
        super();
    }

    public Listener(String nickname) {
        super(nickname);
    }
}
