package mbe.td2015.projectqanda;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import mbe.td2015.projectqanda.bluetooth.BluetoothAPI;
import mbe.td2015.projectqanda.bluetooth.BluetoothCommandService;
import mbe.td2015.projectqanda.common.model.Listener;
import mbe.td2015.projectqanda.common.model.Question;
import mbe.td2015.projectqanda.common.model.Questions;
import mbe.td2015.projectqanda.common.model.Session;
import mbe.td2015.projectqanda.common.security.Authentication;

/**
 * Activité principale de l'application.
 */
public class ConnectionActivity extends ActionBarActivity {
    /**
     * Constantes permettant d'envoyer des donneés entre activités
     */
    public static final String EXTRA_OBJECT_TO_DISPLAY = "object_to_display";
    public static final String EXTRA_OBJECT_CLASSNAME = "classname";

    /**
     * Définission des indices de requêtes
     */
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_AUTHENTICATION_DATAS = 3;

    /**
     * Méta-données de l'auditeur
     */
    public static Listener listener = null;

    /**
     * Utilisation du Bluetooth
     */
    protected BluetoothAPI bApi;

    /**
     * Composant graphique du status de connexion
     */
    private TextView tvConnectionStatus;

    /**
     * Nom de l'ordinateur de connexion
     */
    private String mConnectedDeviceName = null;

    /**
     * Méta-données des questions et de la session
     */
    public static Questions questions;
    public static Session session;

    /**
     * Type d'objet pouvant être reçu par Bluetooth
     */
    public enum ClassType {
        STATUS,
        QUESTIONS,
        QUESTION,
        VOTE,
        ANSWER,
        SESSION;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialisation de l'API Bluetooth
        bApi = BluetoothAPI.getInstance();
        bApi.init(this, mHandler);

        setContentView(R.layout.connection);

        tvConnectionStatus = (TextView) findViewById(R.id.connection_status);
        questions = new Questions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Appel de l'activité d'initialisation du Bluetooth
        if(bApi.isBluetoothSupported())
            bApi.enableBlutooth(REQUEST_ENABLE_BT);
    }

    @Override
    protected void onResume() {
        super.onResume();

        bApi.resumeConnection();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        bApi.stopConnection();
    }

    /**
     * Démarre l'activité de le liste d'appareil
     * @param view
     */
    public void startDeviceListActivity(View view) {
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    /**
     * Récupère le résultat des activités lancées avec
     * startActivityForResult()
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // Quand DeviceListActivity retourne un appareil a connecter
                if (resultCode == Activity.RESULT_OK) {
                    // Récupération de la MAC adresse
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

                    bApi.connect(address);
                }
                break;
            case REQUEST_ENABLE_BT:
                // Quand la requête d'activiation du Bluetooth est de retour
                if (resultCode == Activity.RESULT_OK) {
                    // Si le Bluetooth est maintenant activé, lancement d'une session
                    bApi.setupCommand();
                } else {
                    // L'utilisateur n'a pas activé son Bluetooth
                    // ou une erreur survient
                    finish();
                }
                break;
            case REQUEST_AUTHENTICATION_DATAS:
                // Quand le requête d'autentification retourne des données
                if(data != null) {
                    String password = data.getExtras()
                            .getString(AuthenticationActivity.EXTRA_AUTHENTICATION_PASSWORD);
                    String nickname = data.getExtras()
                            .getString(AuthenticationActivity.EXTRA_AUTHENTICATION_NICKNAME);

                    this.listener = new Listener(nickname);

                    // Création et envois d'un nouvel objet d'authentification
                    Authentication auth = new Authentication(password);
                    bApi.write(auth);
                }
                break;
        }
    }

    /**
     * Gestionnaire
     * ------------
     * Permet au activité ayant accès à ce gestionnaire
     * de retourner des informations à cette activité.
     */
    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                /*
                Gère les différents états de la connexion
                 */
                case BluetoothAPI.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothCommandService.STATE_CONNECTED:
                            askForAuthentication();
                            break;
                        case BluetoothCommandService.STATE_CONNECTING:
                            tvConnectionStatus.setText(R.string.title_connecting);
                            break;
                        case BluetoothCommandService.STATE_LISTEN:
                        case BluetoothCommandService.STATE_NONE:
                            tvConnectionStatus.setText(R.string.title_not_connected);
                            break;
                    }
                    break;

                /*
                Gère la déconnection à l'application desktop
                 */
                case BluetoothCommandService.DISCONNECTED:
                    if(QuestionListActivity.getInstance() != null)
                        QuestionListActivity.getInstance().finish();
                    if(AuthenticationActivity.getInstance() != null)
                        AuthenticationActivity.getInstance().finish();
                    restart();
                    break;

                /*
                Récupère le nom de l'appareil connecté (l'ordinateur)
                 */
                case BluetoothAPI.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(BluetoothAPI.DEVICE_NAME);
                    break;

                /*
                Gère la récéption d'objets
                 */
                case BluetoothAPI.MESSAGE_RECEIVED_OBJECT:
                    // L'objet est reçu en JSon
                    // La librairie Gson de Google permet de sérialiser et désérialiser des
                    // objet JSon.
                    Gson gson = new Gson();

                    // Récupération de l'objet
                    String jsonObject = msg.getData().getString(BluetoothAPI.RECEIVED_OBJECT);
                    // Récupération du nom de la classe de l'objet
                    String classname = msg.getData().getString(BluetoothAPI.RECEIVED_OBJECT_CLASSNAME);

                    ClassType type = ClassType.valueOf(classname.toUpperCase());

                    // Switch sur le type de classe
                    switch (type) {
                        case STATUS:
                            Authentication.Status status = gson.fromJson(jsonObject, Authentication.Status.class);
                            processAuthenticationStatus(status);
                            break;
                        case QUESTIONS:
                            questions = gson.fromJson(jsonObject, Questions.class);
                            // Lors de la récéption de la liste des questions, on reçoit également
                            // des informations de vote sur les questions.
                            // Il est donc nécéssaire de les réinitialisés.
                            questions.reinitIsVoted();
                            break;
                        case QUESTION:
                            Question question = gson.fromJson(jsonObject, Question.class);
                            questions.addQuestion(question);
                            // Envoie de la question a la liste de question
                            startQuestionListActivitiy(question);
                            break;
                        case VOTE:
                            Question.Vote vote = gson.fromJson(jsonObject, Question.Vote.class);
                            // Envoie du vote a la liste de question
                            startQuestionListActivitiy(vote);
                            break;
                        case ANSWER:
                            Question.Answer answer = gson.fromJson(jsonObject, Question.Answer.class);
                            // Envoie de la réponse a la liste de question
                            startQuestionListActivitiy(answer);
                            break;
                        case SESSION:
                            session = gson.fromJson(jsonObject, Session.class);
                            // Envoie de la session a la liste de question
                            startQuestionListActivitiy();
                            break;
                    }
                    break;
            }
        }

        /**
         * Appelé lors d'une perte de connexion
         */
        private void restart() {
            bApi.restart();
            questions = new Questions();
            session = null;
        }

        /**
         * Démarre l'activité d'authentification
         */
        private void askForAuthentication() {
            Intent authentication = new Intent(getBaseContext(), AuthenticationActivity.class);
            authentication.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(authentication, REQUEST_AUTHENTICATION_DATAS);
        }

        /**
         * Traite le status d'authentification reçu
         * @param status
         */
        private void processAuthenticationStatus(Authentication.Status status) {
            if(status == Authentication.Status.CON_SUCCEED) {
                tvConnectionStatus.setText(R.string.title_connected_to);
                tvConnectionStatus.append(mConnectedDeviceName);
            } else {
                askForAuthentication();
            }
        }

        /**
         * Démarre l'activité de la liste de questions
         */
        private void startQuestionListActivitiy() {
            Intent questionList = new Intent(getBaseContext(), QuestionListActivity.class);
            questionList.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(questionList);
        }

        /**
         * Envois un objet à l'activité de la liste de questions
         * @param obj
         */
        private void startQuestionListActivitiy(Object obj) {
            Gson gson = new Gson();
            Intent questionList = new Intent(getBaseContext(), QuestionListActivity.class);

            questionList.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            questionList.putExtra(EXTRA_OBJECT_TO_DISPLAY, gson.toJson(obj));
            questionList.putExtra(EXTRA_OBJECT_CLASSNAME, obj.getClass().getSimpleName());
            startActivity(questionList);
        }
    };
}
