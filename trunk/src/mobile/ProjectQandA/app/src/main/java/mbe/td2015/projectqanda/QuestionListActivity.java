package mbe.td2015.projectqanda;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.ParseException;
import java.util.ArrayList;

import mbe.td2015.projectqanda.bluetooth.BluetoothAPI;
import mbe.td2015.projectqanda.common.model.Question;
import mbe.td2015.projectqanda.common.model.Questions;
import mbe.td2015.projectqanda.ui.QuestionAdapter;

/**
 * Gère la liste de questions
 */
public class QuestionListActivity extends AppCompatActivity {
    /**
     * Debuggage
     */
    private static final String TAG = "QuestionListActivity";

    /**
     * Instance de cette activité (utiliser pour quitter l'activité depuis
     * ConnectionActivity en cas de déconnection avec le serveur)
     */
    static QuestionListActivity questionListActivity;

    /**
     * Liste de questions a afficher
     */
    private QuestionAdapter mQuestionsArrayAdapter;


    /**
     * Instance de l'API Bluetooth
     */
    private BluetoothAPI bApi;

    /**
     * Composant graphique
     */
    private ListView lvQuestions;
    private EditText etQuestion;
    private TextView tvSessionName;
    private TextView tvSessionSpeakerNickname;

    public static QuestionListActivity getInstance() {
        return questionListActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_list);

        questionListActivity = this;

        bApi = BluetoothAPI.getInstance();

        /*
        Initialisation des composants graphique
         */
        etQuestion = (EditText) findViewById(R.id.questionList_etQuestion);

        lvQuestions = (ListView) findViewById(R.id.questionList_lvQuestionList);
        mQuestionsArrayAdapter = new QuestionAdapter(this, R.layout.question, new ArrayList<Question>());
        lvQuestions.setAdapter(mQuestionsArrayAdapter);

        tvSessionName = (TextView) findViewById(R.id.questionList_tvSessionName);
        tvSessionSpeakerNickname = (TextView) findViewById(R.id.questionList_tvSessionSpeakerNickname);

        if(ConnectionActivity.session != null) {
            tvSessionName.setText(ConnectionActivity.session.getName());
            tvSessionSpeakerNickname.setText(ConnectionActivity.session.getSpeaker().getNickname());
        }

        if(ConnectionActivity.questions.count() > 0) {
            displayQuestions(ConnectionActivity.questions);
            mQuestionsArrayAdapter.questions.orderQuestions();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_question_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Lors d'un clic sur le bouton de mise dans l'ordre des quetions
        if (id == R.id.action_order) {
            mQuestionsArrayAdapter.questions.orderQuestions();
            mQuestionsArrayAdapter.notifyDataSetChanged();
        }

        // Lors d'un clic sur le bouton de déconnection
        if (id == R.id.action_disconnect) {
            ConnectionActivity.questions = null;
            bApi.stopConnection();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
        questionListActivity = null;
    }

    /**
     * Lors du lancement de l'activité avec un paramètre.
     * Cette méthode est appeller même si l'activité est déjà démarré.
     * Elle permet de faire transiter des données à cette activité.
     * @param intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        Gson gson = new Gson();
        Bundle extras = intent.getExtras();

        if(extras != null) {
            // Récupération de l'objet
            String jsonObject = extras.getString(ConnectionActivity.EXTRA_OBJECT_TO_DISPLAY);
            String classname = extras.getString(ConnectionActivity.EXTRA_OBJECT_CLASSNAME);

            ConnectionActivity.ClassType type =
                    ConnectionActivity.ClassType.valueOf(classname.toUpperCase());

            // Switch sur le type de classe reçu
            switch (type) {
                case QUESTION:
                    // Affichage de la question
                    Question question = gson.fromJson(jsonObject, Question.class);
                    displayQuestion(question);
                    break;

                case VOTE:
                    // Comptabilisation du vote à la question correspondante
                    Question.Vote receivedVote = gson.fromJson(jsonObject, Question.Vote.class);
                    String uuid = receivedVote.getUuid();
                    Question q = mQuestionsArrayAdapter.questions.getQuestion(uuid);
                    boolean orgVote = q.getVote().isVoted;

                    receivedVote.setIsVoted(orgVote);
                    q.setVote(receivedVote);
                    mQuestionsArrayAdapter.questions.setQuestion(uuid, q);
                    mQuestionsArrayAdapter.notifyDataSetChanged();
                    break;

                case ANSWER:
                    // Comptabilisation de la réponse à la question correspondante
                    Question.Answer receivedAnswer = gson.fromJson(jsonObject, Question.Answer.class);
                    String answeredUuid = receivedAnswer.getUuid();
                    Question answeredQuestion = mQuestionsArrayAdapter.questions.getQuestion(answeredUuid);
                    answeredQuestion.setAnswer(receivedAnswer);
                    mQuestionsArrayAdapter.questions.setQuestion(answeredUuid, answeredQuestion);
                    mQuestionsArrayAdapter.notifyDataSetChanged();
                    break;
            }
        }

    }

    public void displayQuestion(Question question) {
            mQuestionsArrayAdapter.add(question);

    }

    public void displayQuestions(Questions questions) {
        for(Question q : questions.getQuestions()) {
                mQuestionsArrayAdapter.add(q);
        }
    }

    /**
     * Envoie d'une question par Bluetooth
     * @param view
     * @throws ParseException
     */
    public void sendQuestion(View view) throws ParseException {
        // Récupération des données de la question
        EditText etQuestion = (EditText) findViewById(R.id.questionList_etQuestion);
        String question = etQuestion.getText().toString();

        // Valide l'entrée de l'utilisateur
        Integer error = validQuestion(question);

        if (error != null) {
            etQuestion.setHint(error);
        } else {
            // Création de la question
            Question q = new Question(ConnectionActivity.listener, question);
            mQuestionsArrayAdapter.add(q);
            // Envoie de la question
            bApi.write(q);
            this.resetUI();
        }
    }

    private Integer validQuestion(String question) {
        if(question.length() == 0) // La question ne peut pas être vide
            return R.string.questionList_enter_a_question;
        return null;
    }

    /**
     * Réinitialise l'UI après avoir posé une question
     */
    private void resetUI() {
        // Remets à vide le champ de ls question
        etQuestion.setText("");
        // Ferme le clavier virtuel
        closeKeyboard();
    }

    /**
     * Ferme le clavier virtuel
     */
    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * Gère la comptabilisation et l'envois d'un vote
     * @param view
     */
    public void vote(View view) {
        RelativeLayout vwParentRow = (RelativeLayout)view.getParent();
        // Récupération de l'ID de la question
        TextView tvQId = (TextView) vwParentRow.getChildAt(0);
        // Récupération de la checkBox du vote
        CheckBox cbVote = (CheckBox)vwParentRow.getChildAt(4);

        String uuid = tvQId.getText().toString();
        Question question = mQuestionsArrayAdapter.questions.getQuestion(uuid);
        if(cbVote.isChecked())
            question.vote();
        else
            question.unvote();
        // Envoie du vote
        bApi.write(question.getVote());
        // Mise à jour du nombre de votes
        cbVote.setText(question.getVote().getVoteCounter().toString());
        // Mise à jour de la question
        mQuestionsArrayAdapter.questions.setQuestion(uuid, question);
    }
}
