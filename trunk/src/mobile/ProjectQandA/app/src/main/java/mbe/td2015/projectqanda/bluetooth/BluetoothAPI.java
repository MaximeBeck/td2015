package mbe.td2015.projectqanda.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;

import java.util.Set;

/**
 * API Bluetooth (Classe Standalone)
 */
public class BluetoothAPI {
    /**
     * Instance de la classe standalone
     */
    private static BluetoothAPI instance = null;

    /**
     * Constantes d'états
     * ------------------
     * Permet de retourner des données au Handler de ConnectionActivity
     */
    public static final int MESSAGE_STATE_CHANGE = 100;
    public static final int MESSAGE_DEVICE_NAME = 101;
    public static final int MESSAGE_TOAST = 102;
    public static final int MESSAGE_RECEIVED_OBJECT = 103;

    /**
     * Mots clé des données reçu du Handler de ConnectionActivity
     */
    public static final String TOAST = "toast";
    public static final String DEVICE_NAME = "device_name";
    public static final String RECEIVED_OBJECT = "received_object";
    public static final String RECEIVED_OBJECT_CLASSNAME = "received_object_classname";

    /**
     * Adaptateur Bluetooth
     */
    public BluetoothAdapter adapter = null;

    /**
     * Instance du Service de Commande Bluetooth
     */
    protected BluetoothCommandService mCommandService = null;

    /**
     * Context de l'application
     */
    private Activity context = null;

    /**
     * Instance du Handler auquel retourner les données (ConnectionActivity)
     */
    private Handler mHandler;

    private BluetoothAPI() {

    }

    public static BluetoothAPI getInstance() {
        if(instance == null){
            instance = new BluetoothAPI();
        }
        return instance;
    }

    public void init(Activity context, Handler handler) {
        if(adapter == null) {
            this.context = context;
            this.mHandler = handler;
            // Récupération de l'adaptateur Bluetooth local
            adapter = BluetoothAdapter.getDefaultAdapter();
            this.setupCommand();
        }
    }

    /**
     * Appeler lors d'un redémarrage de l'API Bluetooth (lors d'une coupure
     * de connexion ou d'une déconnection par exemple)
     */
    public void restart() {
        // Récupération d'un nouvel adaptateur Bluetooth local
        adapter = BluetoothAdapter.getDefaultAdapter();
    }

    /**
     * Récupère les appareils appairés
     * @return
     */
    public Set<BluetoothDevice> getPairedDevices() {
        return this.adapter.getBondedDevices();
    }

    /**
     * Vérifie que le Bluetooth soit supporter sur l'appareil
     * @return
     */
    public boolean isBluetoothSupported() {
        // Si l'adaptateur est null, le Bluetooth n'est pas supporté
        if (adapter == null)
            return false;
        return true;
    }

    /**
     * Lance l'activation du Bluetooth sur l'appareil
     * @param requestCode
     */
    public void enableBlutooth(int requestCode) {
        // Si le Bluetooth n'est pas déjà activé,
        // demander son activation
        if (!adapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            context.startActivityForResult(enableIntent, requestCode);
        }
        // sinon, démarre le service de commande
        else {
            if (mCommandService == null)
                setupCommand();
        }
    }

    /**
     * Tente une connexion à l'appareil distant
     * @param address
     */
    public void connect(String address) {
        // Récupération de l'appareil Bluetooth distant
        BluetoothDevice device = adapter.getRemoteDevice(address);
        // Tente de ce connecter à l'appareil
        mCommandService.connect(device);
    }

    /**
     * Appelez cette méthode dans le onResume().
     * Celle-ci couvre le cas ou le Bluetooth n'a pas été démarré
     * lors du onStart().
     */
    public void resumeConnection() {
        if (mCommandService != null) {
            if (mCommandService.getState() == BluetoothCommandService.STATE_NONE) {
                mCommandService.start();
            }
        }
    }

    /**
     * Arrête la connexion en cours
     */
    public void stopConnection() {
        if (mCommandService != null)
            mCommandService.stop();
    }

    /**
     * Permet de rendre l'appareil Bluetooth local visible
     */
    public void ensureDiscoverable() {
        if (adapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            context.startActivity(discoverableIntent);
        }
    }

    /**
     * Envoie par Bluetooth d'une string
     * @param message
     */
    public void write(String message) {
        mCommandService.write(message);
    }

    /**
     * Envoie par Bluetooth d'un objet
     * @param obj
     */
    public void write(Object obj) {
        mCommandService.write(obj);
    }

    /**
     * Envoie par Bluetooth d'un ArrayByte
     * @param data
     */
    public void write(byte[] data) {
        mCommandService.write(data);
    }

    /**
     * Envoie par Bluetooth d'un int
     * @param number
     */
    public void write(int number) {
        mCommandService.write(number);
    }

    /**
     * Crée une nouvelle instance du Service de Commandes Bluetooth
     */
    public void setupCommand() {
        mCommandService = new BluetoothCommandService(mHandler);
    }

}
