package mbe.td2015.projectqanda.common.model;

import java.io.Serializable;

/**
 * Orateur
 */
public class Speaker extends User implements Serializable {
    public Speaker() {
        super();
    }

    public Speaker(String nickname) {
        super(nickname);
    }
}
