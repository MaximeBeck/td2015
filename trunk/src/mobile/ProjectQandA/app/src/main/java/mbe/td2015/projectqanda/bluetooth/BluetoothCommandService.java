package mbe.td2015.projectqanda.bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.commons.lang.ArrayUtils;

import mbe.td2015.projectqanda.common.util.ByteArraySerializer;

/**
 * Service de Commande Bluetooth
 * -----------------------------
 * Gère toute la logique liée aux manipulations Bluetooth.
 * Contient les threads de connexion en entrée et en sortie
 */
public class BluetoothCommandService {
    /**
     * Debugging
     */
    private static final String TAG = "BluetoothCommandService";
    private static final boolean D = true;

    /**
     * UUID de la connexion
     */
    private static final UUID MY_UUID = UUID.fromString("04c6093b-0000-1000-8000-00805f9b34fb");


    /**
     * Adaptateur Bluetooth
     */
    private final BluetoothAdapter mAdapter;

    /**
     * Instance du Handler auquel retourner les données (ConnectionActivity)
     */
    private final Handler mHandler;

    /**
     * Thread de connexion
     */
    private ConnectThread mConnectThread;

    /**
     * Thread connecté
     */
    private ConnectedThread mConnectedThread;

    /**
     * État de la connexion
     */
    private int mState;

    /**
     * Constantes décrivant les états de connexion possible
     */
    public static final int STATE_NONE = 0;       // on ne fait rien
    public static final int STATE_LISTEN = 1;     // on écoute les connexion en entrée
    public static final int STATE_CONNECTING = 2; // on initialise une connexion en sortie
    public static final int STATE_CONNECTED = 3;  // on est connecté à l'appareil distant
    public static final int DISCONNECTED = 4;     // l'appareil distant a été déconnecté

    /**
     * Constante décrivant le type d'objet reçu
     */
    public static final int EXIT_CMD = -1;
    public static final int OBJECT = 0;
    public static final int STRING = 1;

    /**
     * Constructeur. Prépare une nouvelle session Bluetooth.
     * @param handler
     */
    public BluetoothCommandService(Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
    }


    /**
     * Mets en place l'état courant de la connexion
     * @param state
     */
    private synchronized void setState(int state) {
        if (D) Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Envoie de l'état actuelle au Handler
        if (mHandler != null)
            mHandler.obtainMessage(BluetoothAPI.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Retourne l'état courrant de la connexion.
     */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Démarre les threads de connexion.
     */
    public synchronized void start() {
        if (D) Log.d(TAG, "start");

        // Annule toutes les threads qui tente de se connecter
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Annule toutes les threads qui sont connectées
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_LISTEN);
    }

    /**
     * Démarre ConnectThread pour initialiser la connexion à un appareil distant.
     * @param device
     */
    public synchronized void connect(BluetoothDevice device) {
        if (D) Log.d(TAG, "connect to: " + device);

        // Annule toutes les threads qui tente de se connecter
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Annule toutes les threads qui sont connectées
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Démarre la thread de connexion à un appareil
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Démarre ConnectedThread pour commencer la gestion de la connexion Bluetooth
     * @param socket
     * @param device
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        if (D) Log.d(TAG, "connected");

        // Annule toutes les threads qui tente de se connecter
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Annule toutes les threads qui sont connectées
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Démarre la thread de gestion de la connexion Bluetooth
        mConnectedThread = new ConnectedThread(socket);

        new Thread(mConnectedThread).start();

        // Envoie le nom de l'appareil connecté au Handler
        if (mHandler != null) {
            Message msg = mHandler.obtainMessage(BluetoothAPI.MESSAGE_DEVICE_NAME);
            Bundle bundle = new Bundle();
            bundle.putString(BluetoothAPI.DEVICE_NAME, device.getName());
            msg.setData(bundle);
            mHandler.sendMessage(msg);

            setState(STATE_CONNECTED);
        }
    }

    /**
     * Arrête toutes les threads
     */
    public synchronized void stop() {
        if (D) Log.d(TAG, "stop");
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_NONE);
    }

    /**
     * Envoie les données par Bluetooth
     *
     * @param out
     */
    public void write(byte[] out) {
        // Crée un objet
        ConnectedThread r;
        // Synchronise une copie de ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Réalise l'écriture non-synchronisé
        r.write(out);
    }

    public void write(int out) {
        ConnectedThread r;
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        r.write(out);
    }

    public void write(Object obj) {
        byte[] bObj = ByteArraySerializer.serialize(obj);
        int nbBytesInObject = bObj.length;
        byte[] bNb = ByteBuffer.allocate(4).putInt(nbBytesInObject).array();
        int nbBytesInNb = bNb.length;

        write(OBJECT);
        write(nbBytesInNb);
        write(bNb);
        write(bObj);
    }

    public void write(Object obj, int tag) {
        byte[] bObj = ByteArraySerializer.serialize(obj);
        int nbBytesInObject = bObj.length;
        byte[] bNb = ByteBuffer.allocate(4).putInt(nbBytesInObject).array();
        int nbBytesInNb = bNb.length;

        write(tag);
        write(nbBytesInNb);
        write(bNb);
        write(bObj);
    }

    public void write(String str) {
        byte[] b = str.getBytes();
        int nbBytesInString = str.length();

        write(STRING);
        write(nbBytesInString);
        write(b);
    }

    /**
     * Indique que la connexion a échouée
     */
    private void connectionFailed() {
        setState(STATE_LISTEN);

        // Envoie un message d'échec au Handler
        if (mHandler != null) {
            Message msg = mHandler.obtainMessage(BluetoothAPI.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(BluetoothAPI.TOAST, "Unable to connect device");
            msg.setData(bundle);
            mHandler.sendMessage(msg);
        }
    }

    /**
     * Indique que la connexion a été perdue
     */
    private void connectionLost() {
        setState(STATE_LISTEN);
        if (mHandler != null) {
            mHandler.obtainMessage(DISCONNECTED, 0, -1).sendToTarget();
        }
    }

    /**
     * Cette thread s'exécute en tentant de crée une connexion
     * avec un appareil.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "create() failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectThread");
            setName("ConnectThread");

            mAdapter.cancelDiscovery();

            try {
                mmSocket.connect();
            } catch (IOException e) {
                connectionFailed();
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "unable to close() socket during connection failure", e2);
                }
                BluetoothCommandService.this.start();
                return;
            }

            synchronized (BluetoothCommandService.this) {
                mConnectThread = null;
            }
            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    /**
     * Cette thread s'exécute durant une connexiono avec un appareil distant.
     * Elle gère toute les transmissions en entrée et en sortie.
     */
    private class ConnectedThread extends Observable implements Runnable {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "create ConnectedThread");
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");

            while (true) {
                try {
                    int c = mmInStream.read();

                    switch (c) {
                        case OBJECT:
                            this.readObject();
                            break;
                        case STRING:
                            String message = this.readString(mmInStream);
                            setChanged();
                            notifyObservers(message);
                            break;
                        case EXIT_CMD:
                            break;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                    connectionLost();
                    break;
                }
            }
        }

        /**
         *  Récupère l'objet reçu.
         *
         *  Trame de réception d'un élément :
         *  *******************************************
         *  * INT  *    INT    *    BYTE     *  BYTE  *
         *  *******************************************
         *  * ENUM * TYPE_SIZE * OBJECT_SIZE * OBJECT *
         *  *******************************************
         */
        private void readObject() throws IOException {
            List<Byte> bObject = new ArrayList<Byte>();
            List<Byte> bObjectSize = null;
            int typeSize = -1, objectSize = -1, c;
            boolean streaming = true;

            try {
                while (streaming) {
                    c = mmInStream.read();

                    if (typeSize == -1) {
                        typeSize = c;
                    }

                    if (bObjectSize == null) {
                        bObjectSize = new ArrayList<Byte>();
                        for (int i = 0; i < typeSize; i++) {
                            c = mmInStream.read();
                            bObjectSize.add(new Byte((byte) c));
                        }
                        Byte[] bytes = bObjectSize.toArray(new Byte[bObjectSize.size()]);
                        objectSize = ByteBuffer.wrap(ArrayUtils.toPrimitive(bytes)).getInt();
                    }

                    if (objectSize != -1) {
                        for (int i = 0; i < objectSize; i++) {
                            c = mmInStream.read();
                            bObject.add(new Byte((byte) c));
                        }
                        Byte[] bytes = bObject.toArray(new Byte[bObject.size()]);
                        processCommand(ArrayUtils.toPrimitive(bytes));
                        streaming = false;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Récupère la String reçu
         *
         * @param inputStream
         * @return
         * @throws IOException
         */
        private String readString(InputStream inputStream) throws IOException {
            boolean streaming = true;
            String strResult = "";
            int c;

            while (streaming) {
                int size = inputStream.read();

                for (int i = 0; i < size; i++) {
                    c = inputStream.read();
                    strResult += (char) c;
                }
                streaming = false;
            }
            return strResult;
        }

        /**
         * Retourne au Handler les données reçues
         * @param data
         */
        private void processCommand(byte[] data) {
            try {
                final Object obj = ByteArraySerializer.deserialize(data);
                Gson gson = new Gson();

                Message msg = mHandler.obtainMessage(BluetoothAPI.MESSAGE_RECEIVED_OBJECT);
                Bundle bundle = new Bundle();
                bundle.putString(BluetoothAPI.RECEIVED_OBJECT, gson.toJson(obj));
                bundle.putString(BluetoothAPI.RECEIVED_OBJECT_CLASSNAME, obj.getClass().getSimpleName());
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Envoie de données à l'appareil distant
         * @param buffer
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void write(int out) {
            try {
                mmOutStream.write(out);
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mmOutStream.write(EXIT_CMD);
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }
}