package mbe.td2015.projectqanda.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mbe.td2015.projectqanda.R;
import mbe.td2015.projectqanda.common.model.Question;
import mbe.td2015.projectqanda.common.model.Questions;

/**
 * Gère l'affichage de la liste de questions
 */
public class QuestionAdapter extends ArrayAdapter<Question> {
    /**
     * Contexte de l'application
     */
    private final Context context;

    /**
     * ID du composant graphique
     */
    private final int layoutResourceId;

    /**
     * Liste de questions
     */
    public Questions questions;

    public QuestionAdapter(Context context, int layoutResourceId, ArrayList<Question> questions) {
        super(context, layoutResourceId, questions);
        this.context = context;
        this.questions = new Questions(questions);
        this.layoutResourceId = layoutResourceId;
    }

    /**
     * Gère l'affichage des questions
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*
        Gestion des données cellule par cellule
         */
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();

            // Récupération des composants graphique de la cellule
            holder.rlQuestion = (RelativeLayout) row.findViewById(R.id.rlQuestion);
            holder.tvQId = (TextView) row.findViewById(R.id.tvQId);
            holder.tvAuthor = (TextView) row.findViewById(R.id.tvAuthor);
            holder.tvQuestion = (TextView) row.findViewById(R.id.tvQuestion);
            holder.tvTime = (TextView) row.findViewById(R.id.tvTime);
            holder.cbVote = (CheckBox) row.findViewById(R.id.cbVote);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        // Récupération d'une question
        Question question = questions.getQuestion(position);
        Question.Vote vote = question.getVote();
        Question.Answer answer = question.getAnswer();

        // Mise en place dans la cellule des données de la question en cours
        if(answer.isAnswered)
            holder.rlQuestion.setBackgroundColor(Color.rgb(0,113,113));
        else
            holder.rlQuestion.setBackgroundColor(Color.rgb(228,228,228));
        holder.tvQId.setText(question.getUuid());
        holder.tvAuthor.setText(question.getListener().getNickname());
        holder.tvQuestion.setText(question.getBody());
        holder.tvTime.setText(question.getTime());
        holder.cbVote.setText(vote.getVoteCounter().toString());
        if(vote.isVoted)
            holder.cbVote.setChecked(true);
        else
            holder.cbVote.setChecked(false);

        return row;
    }

    // Composants graphique qui compose une cellule
    static class ViewHolder {
        RelativeLayout rlQuestion;
        TextView tvQId;
        TextView tvAuthor;
        TextView tvQuestion;
        TextView tvTime;
        CheckBox cbVote;
    }
}
