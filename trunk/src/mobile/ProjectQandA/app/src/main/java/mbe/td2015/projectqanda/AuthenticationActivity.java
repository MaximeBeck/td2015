package mbe.td2015.projectqanda;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Gère l'authentification de l'utilisateur à l'ordinateur
 */
public class AuthenticationActivity extends Activity {
    /**
     * Permet l'envoie de données à d'autres activités
     */
    public static final String EXTRA_AUTHENTICATION_NICKNAME = "authentication_nickname";
    public static final String EXTRA_AUTHENTICATION_PASSWORD = "authentication_password";

    /**
     * Instance de cette activité (utiliser pour quitter l'activité depuis
     * ConnectionActivity en cas de déconnection avec le serveur)
     */
    static AuthenticationActivity authenticationActivity;

    private EditText edNickname;
    private EditText edPassword;

    public static AuthenticationActivity getInstance() {
        return authenticationActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentication);

        authenticationActivity = this;

        edNickname = (EditText) findViewById(R.id.authentication_etNickname);
        edPassword = (EditText) findViewById(R.id.authentication_etPassword);

        // Récupération du pseudo de l'auditeur s'il est déjà défini
        if (ConnectionActivity.listener != null) {
            edNickname.setText(ConnectionActivity.listener.getNickname());
            edPassword.requestFocus();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {getMenuInflater().inflate(R.menu.menu_authentication, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        authenticationActivity = null;
    }

    /**
     * Retoure des donnnées d'authentification à ConnectionActivity
     * @param view
     */
    public void sendAuthenticationData(View view) {
        TextView tvErrorMessage = (TextView) findViewById(R.id.authentication_tvErrorMessage);

        String nickname = edNickname.getText().toString();
        String password = edPassword.getText().toString();

        // Valide si les données sont correctes
        Integer error = validAuthenticationData(nickname, password);

        if (error != null) {
            tvErrorMessage.setText(error);
        } else {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_AUTHENTICATION_NICKNAME, nickname);
            intent.putExtra(EXTRA_AUTHENTICATION_PASSWORD, password);

            // Set result and finish this Activity
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    private Integer validAuthenticationData(String nickname, String password) {
        final int PASSWORD_LENGTH = 6;
        if (nickname.length() == 0) // Le pseudo est obligatoire
            return R.string.authentication_nickname_is_empty;
        if (!nickname.matches("[A-Za-z0-9 ]+")) // Le pseudo doit être alphanumerique
            return R.string.authentication_nickname_not_alphanumeric;
        if (!password.matches("[A-Za-z0-9]+")) // Le code doit être alphanumerique
            return R.string.authentication_password_not_alphanumeric;
        if (password.length() != PASSWORD_LENGTH) // Le code à une taille défini
            return R.string.authentication_password_not_correct_length;
        return null;
    }
}
