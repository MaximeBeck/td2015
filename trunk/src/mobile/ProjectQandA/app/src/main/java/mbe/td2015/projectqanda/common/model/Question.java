package mbe.td2015.projectqanda.common.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

/**
 * Question
 */
public class Question implements Serializable, Comparator<Question> {
    /**
     * Format par défaut de l'heure de récéption d'une question
     */
    private static final String DEFAULT_TIME_FORMAT = "HH:mm";

    private String uuid;
    private Listener listener;
    private String body;
    private String time;
    private Vote vote;
    private Answer answer;

    public Question() {

    }

    public Question(Listener listener, String body) {
        this.uuid = UUID.randomUUID().toString();
        this.listener = listener;
        this.body = body;
        this.time = getFormatedTime();
        this.vote = new Vote(uuid);
        this.answer = new Answer(uuid);
    }

    /**
     * Retourne l'heure de réception de la question
     * formaté en String
     * @return
     */
    private String getFormatedTime() {
        Date currentTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
        return sdf.format(currentTime);
    }

    public void vote() {
        this.vote.plusOne();
    }

    public void unvote() {
        this.vote.minusOne();
    }

    public void answer() {
        this.answer.setIsAnswered(true);
    }

    public void unanswer() {
        this.answer.setIsAnswered(false);
    }

    public String getUuid() {
        return uuid;
    }

    public String getTime() {
        return time;
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Vote getVote() {
        return vote;
    }

    public void setVote(Vote vote) {
        this.vote = vote;
    }

    @Override
    public String toString() {
        return this.listener.getNickname() + "\n" +
               this.body + "\n" +
               this.getTime();
    }

    @Override
    public int compare(Question q1, Question q2) {
        return q1.vote.getVoteCounter().compareTo(q2.vote.getVoteCounter());
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    /**
     * Vote
     */
    public class Vote implements Serializable {
        private String uuid;
        public Boolean isVoted;
        private int voteCounter;

        public Vote(String uuid) {
            this.uuid = uuid;
            this.isVoted = false;
        }

        public void plusOne() {
            voteCounter++;
            isVoted = true;
        }

        public void minusOne() {
            voteCounter--;
            isVoted = false;
        }

        public Integer getVoteCounter() {
            return voteCounter;
        }

        public void setIsVoted(Boolean isVoted) {
            this.isVoted = isVoted;
        }

        public String getUuid() {
            return uuid;
        }
    }

    /**
     * Réponse
     */
    public class Answer implements Serializable {
        private String uuid;
        public Boolean isAnswered;

        public Answer(String uuid) {
            this.uuid = uuid;
            this.isAnswered = false;
        }

        public Answer(String uuid, boolean isAnswered) {
            this.uuid = uuid;
            this.isAnswered = isAnswered;
        }

        public void setIsAnswered(Boolean isAnswered) {
            this.isAnswered = isAnswered;
        }

        public String getUuid() {
            return uuid;
        }
    }
}
