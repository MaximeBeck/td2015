package mbe.td2015.projectqanda;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Set;

import mbe.td2015.projectqanda.bluetooth.BluetoothAPI;

/**
 * Activité de découverte d'appareils Bluetooth
 * aux alentoures.
 */
public class DeviceListActivity extends Activity {
    /**
     * Debuggage
     */
    private static final String TAG = "DeviceListActivity";
    private static final boolean D = true;

    /**
     * Permet d'envois des données vers d'autres activités
     */
    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    /**
     * Instance de l'API Bluetooth
     */
    private BluetoothAPI bApi = null;

    /**
     * Instance des listes d'ppareils appairées et découverts
     */
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;
    private ArrayAdapter<String> mNewDevicesArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Récupération de l'instance de l'API Bluetooth
        bApi = BluetoothAPI.getInstance();

        // Mise en place de la fenêtre
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.device_list);

        // Annulation de l'activité si l'utilisateur décide de là
        // quitter
        setResult(Activity.RESULT_CANCELED);

        // Initialise le boutton de découverte des appareils aux alentours
        Button scanButton = (Button) findViewById(R.id.button_scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                doDiscovery();
                v.setVisibility(View.GONE);
            }
        });

        // Initialise les array adapters.
        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);
        mNewDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);

        // Récupère et met en place la liste des appareils appairés
        ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        // Récupère et met en place la liste des nouveaux appareils
        ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
        newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(mDeviceClickListener);

        // Permet d'être notifier lorsqu'un appareil est découvert
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Permet d'être notifier lorsque la découverte est terminé
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);

        // Récupération des appareils déjà appairés
        Set<BluetoothDevice> pairedDevices = bApi.getPairedDevices();

        // S'il y a des appareils déjà appairés, les ajouter à la liste
        if (pairedDevices.size() > 0) {
            findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
            for (BluetoothDevice device : pairedDevices) {
                mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        } else {
            String noDevices = getResources().getText(R.string.none_paired).toString();
            mPairedDevicesArrayAdapter.add(noDevices);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Annuler la découverte si elle est en cours
        if (bApi.adapter != null) {
            bApi.adapter.cancelDiscovery();
        }

        // Déconnexion des notifications
        this.unregisterReceiver(mReceiver);
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        if (D) Log.d(TAG, "doDiscovery()");

        // Indiquer le scannage dans le titre
        setProgressBarIndeterminateVisibility(true);
        setTitle(R.string.scanning);

        findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);

        // Terminé la découverte si elle est déjà en cours
        if (bApi.adapter.isDiscovering()) {
            bApi.adapter.cancelDiscovery();
        }

        // Starter la découverte
        bApi.adapter.startDiscovery();
    }

    /**
     * L'événement onClick pour tout les appareils de la liste
     */
    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Annulation de la découverte
            bApi.adapter.cancelDiscovery();

            // Récupère la MAC adresse de l'appareil.
            // Qui sont les 17 derniers caractères.
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            // Création du résultat
            Intent intent = new Intent();
            intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

            // Envois du résultat et terminer l'activité
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };

    /**
     * Récepteur qui écoute pour la réception d'appareils découvert
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // Lors qu'un appareil est trouvé
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Récupération de l'appareil
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // S'il est déjà appairé, ne pas le prendre en compte car il est déjà listé
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    mNewDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                }
                // Lorsque la découverte est terminé, changer le titre de l'activité
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                setTitle(R.string.select_device);
                if (mNewDevicesArrayAdapter.getCount() == 0) {
                    String noDevices = getResources().getText(R.string.none_found).toString();
                    mNewDevicesArrayAdapter.add(noDevices);
                }
            }
        }
    };
}
