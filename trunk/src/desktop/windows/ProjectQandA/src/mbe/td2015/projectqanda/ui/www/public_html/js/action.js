/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Operations */
function displayQuestion (question) {
    var tmp;
    tmp = "<div id='" + question.getUuid() + "' class='question'>";
    tmp += "<div class='question-infos-area'>";
    tmp += "<div>";
    tmp += "<h2 class='question-author'>" + question.getListener().getNickname() + "</h2>";
    tmp += "<p class='question-body'>" + question.getBody() + "</p>";
    tmp += "</div>";
    tmp += "<div>";
    tmp += "<div class='question-time'>" + question.getTime() + "</div>";
    tmp += "<img class='question-thumbs-up' src='res/voteCounter.png' alt='vote'/>";
    tmp += "<div class='question-vote'> " + question.getVote().getVoteCounter() + "</div>";
    tmp += "</div>";
    tmp += "</div>";
    tmp += "<div class='question-isAnswered-area'>";
    if(!question.getAnswer().isAnswered)
        tmp += "<a class='question-isAnswered' onclick='updateIsAnswered(this.parentNode.parentNode.id)'>&#10003</a>";
    else
        tmp += "<a class='question-isAnswered clicked' onclick='updateIsAnswered(this.parentNode.parentNode.id)'>&#10003</a>";
    tmp += "</div>";
    tmp += "</div>";       
        
    document.getElementById('questions').innerHTML += tmp;        
}

function displayNumberOfConnectedListeners (number) {
    document.getElementById('connected-listeners').innerHTML = number;
}

function displayNumberOfQuestionsAsked (number) {
    document.getElementById('questions-asked').innerHTML = number;
}

function displayNumberOfQuestionsAnswered (number) {
    document.getElementById('questions-answered').innerHTML = number;
}

function displayPassword (password) {
    document.getElementById('password-text').innerHTML = password;
}

/*
function displaySessionInfo (session) {
    document.getElementById('session-name').innerHTML = session.getName();
    document.getElementById('session-speaker-nickname').innerHTML = session.getSpeaker().getNickname();    
}
*/

function updateVoteCounter(qid, counter) {
    voteCtrl = document.getElementById(qid).getElementsByClassName('question-vote')[0];
    voteCtrl.innerHTML = " " + counter;
}

function cleanQuestions () {
    document.getElementById('questions').innerHTML = "";
}

function updateIsAnswered (id) {
    var isAnsweredCtrl = document.getElementById(id).getElementsByClassName('question-isAnswered')[0],
        className = isAnsweredCtrl.className,
        clicked = "clicked";

    if(className.indexOf(clicked) > -1) {
        isAnsweredCtrl.className = isAnsweredCtrl.className.replace( /(?:^|\s)clicked(?!\S)/g , '' );
        java.questionAnswered(id, false);
    } else {
        isAnsweredCtrl.className += " " + clicked;
        java.questionAnswered(id, true);
    }
}

function checkSpeakerInfo () {
    var sessionSpeakerNickname = document.getElementById('session-speaker-nickname-txt').value,
        sessionName = document.getElementById('session-name-txt').value,
        sessionErreur = document.getElementById('session-erreur');

    if(sessionSpeakerNickname !== "" && sessionName !== "") 
        java.btnStartSessionOnClick(sessionName, sessionSpeakerNickname);
    else
        sessionErreur.innerHTML = "* Veuillez remplir les deux champs";
}

// Permet de lancer la session en pressant sur le bouton Enter
// (plutôt que de cliquer sur le bouton "Commencer")
function searchKeyPress(e) {
    e = e || window.event;
    if (e.keyCode == 13)
    {
        document.getElementById('session-start-btn').click();
        return false;
    }
    return true;
}