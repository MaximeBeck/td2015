package mbe.td2015.projectqanda.common.security;

import java.io.Serializable;
import java.util.UUID;

/**
 * Authentification
 */
public class Authentication implements Serializable {
    public enum Status {
        CON_SUCCEED,
        CON_FAILED
    }

    private String password;

    public Authentication() {
        this.password = genNewPassword();
    }

    public Authentication(String password) {
        this.password = password;
    }

    public Status isValid(String password) {
        if(password.equals(this.password))
            return Status.CON_SUCCEED;
        return Status.CON_FAILED;
    }

    private String genNewPassword() {
        String uuid = UUID.randomUUID().toString();
        return uuid.substring(0,6);
    }

    public String getPassword(){
        return this.password;
    }
}