package mbe.td2015.projectqanda.model;

import mbe.td2015.projectqanda.ProcessOutcomingThread;

public class ConnectedDevice {
    /**
     * Instance de l'appareil
     */
    private RemoteMobile remoteMobile;

    /**
     * Instance de sa thraed de connexion en sortie
     */
    private ProcessOutcomingThread processOutcomingThread;

    public ConnectedDevice(RemoteMobile mRemoteMobile, ProcessOutcomingThread mProcessOutcomingThread) {
        this.remoteMobile = mRemoteMobile;
        this.processOutcomingThread = mProcessOutcomingThread;
    }

    public void setIsAuthenticated(boolean isAuthenticated){
        remoteMobile.setIsAuthenticated(isAuthenticated);
    }

    public RemoteMobile getRemoteMobile() {
        return remoteMobile;
    }

    public void setRemoteMobile(RemoteMobile remoteMobile) {
        this.remoteMobile = remoteMobile;
    }

    public ProcessOutcomingThread getProcessOutcomingThread() {
        return processOutcomingThread;
    }

    public void setProcessOutcomingThread(ProcessOutcomingThread processOutcomingThread) {
        this.processOutcomingThread = processOutcomingThread;
    }
}
