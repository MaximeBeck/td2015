package mbe.td2015.projectqanda.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Questions
 */
public class Questions implements Serializable {
    /**
     * Liste de questions
     */
    private ArrayList<Question> questions;

    public Questions() {
        questions = new ArrayList<Question>();
    }

    public Questions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public void reinitIsVoted() {
        Question question = null;
        for(int i = 0; i < questions.size(); i++) {
            question = questions.get(i);
            question.getVote().setIsVoted(false);
            questions.set(i, question);
        }
    }

    public void addQuestion(Question question) {
        this.questions.add(question);
    }

    public int count() {
        return questions.size();
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public int getQuestionIndex(String uuid) {
        for (int i = 0; i < questions.size(); i++) {
            if (questions.get(i).getUuid().equals(uuid))
                return i;
        }
        return -1;
    }

    public Question getQuestion(String uuid) {
            Iterator iter = questions.iterator();
            while (iter.hasNext()) {
                Question q = (Question) iter.next();
                if (q.getUuid().equals(uuid))
                    return q;
            }
            return null;
    }

    public Question getQuestion(int index) {
        return questions.get(index);
    }

    public void setQuestion(String uuid, Question question) {
        int index = getQuestionIndex(uuid);
        questions.set(index, question);
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public void orderQuestions() {
        Collections.sort(questions, new Question());
        Collections.reverse(questions);
    }
}
