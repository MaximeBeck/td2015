package mbe.td2015.projectqanda;

import mbe.td2015.projectqanda.common.model.Question;
import mbe.td2015.projectqanda.common.security.Authentication;
import mbe.td2015.projectqanda.model.ConnectedDevice;
import mbe.td2015.projectqanda.model.RemoteMobile;
import mbe.td2015.projectqanda.ui.Bridge;
import mbe.td2015.projectqanda.common.util.ByteArraySerializer;
import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.bluetooth.RemoteDevice;
import javax.microedition.io.Connection;
import javax.microedition.io.StreamConnection;

/**
 * Thread de gestion de la connexion en entr�e avec un appareil
 */
public class ProcessConnectionThread implements Runnable {
    /**
     * Constance qui indique le type d'objet re�u
     */
    public static final int EXIT_CMD = -1;
    public static final int OBJECT = 0;
    public static final int STRING = 1;

    /**
     * Instance de connexion
     */
    private StreamConnection mConnection;

    /**
     * Flux de donn�es en entr�e
     */
    private InputStream inputStream;

    /**
     * Instance de l'appaeil connect�
     */
    private RemoteMobile remoteMobile;

    /**
     * Donn�es d'authentification
     */
    private Authentication pass;

    /**
     * Instance du pont d'acc�s � l'interface web
     */
    private Bridge bridge;

    /**
     * Type de classes supporter � la r�ception
     */
    private enum ClassType {
        AUTHENTICATION,
        VOTE,
        QUESTION;
    }

    public ProcessConnectionThread(StreamConnection connection, Authentication pass) throws IOException {
        mConnection = connection;
        remoteMobile = new RemoteMobile(RemoteDevice.getRemoteDevice((Connection) connection));
        this.bridge = Bridge.getInstance();
        this.pass = pass;
    }

    @Override
    public void run() {
        try {
            boolean running = true;

            // Attente de la r�ception de donn�es
            inputStream = mConnection.openInputStream();
            while (running) {
                int c = inputStream.read();

                // Traitement de donn�e par type
                switch (c) {
                    case OBJECT:
                        Object obj = this.readObject();
                        processCommand(obj);
                        break;
                    case STRING:
                        // unused
                        break;
                    case EXIT_CMD:
                        try {
                            // Suppression de l'appareil d�connecter
                            for (ConnectedDevice connectedDevice : WaitThread.connectedDevices) {
                                if (connectedDevice.getRemoteMobile().getDevice().equals(remoteMobile.getDevice())) {
                                    WaitThread.connectedDevices.remove(connectedDevice);
                                }
                            }
                        } catch (Exception e) {
                            // Ne rien faire
                        }
                        running = false;
                        break;
                }
            }
            System.out.println(remoteMobile.getDevice().getBluetoothAddress() + " has been deconnected");
            // Mise � jour du nombre d'appareils connect�s
            bridge.displayNumberOfConnectedListeners(WaitThread.connectedDevices.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  R�cup�re l'objet re�u.
     *
     *  Trame de r�ception d'un �l�ment :
     *  *******************************************
     *  * INT  *    INT    *    BYTE     *  BYTE  *
     *  *******************************************
     *  * ENUM * TYPE_SIZE * OBJECT_SIZE * OBJECT *
     *  *******************************************
     */
    private Object readObject() throws IOException {
        List<Byte> bObject = new ArrayList<Byte>();
        List<Byte> bObjectSize = null;
        int typeSize = -1, objectSize = -1, c;
        boolean streaming = true;

        try {
            while (streaming) {
                c = inputStream.read();

                if (typeSize == -1) {
                    typeSize = c;
                }
                if (bObjectSize == null) {
                    bObjectSize = new ArrayList<Byte>();
                    for (int i = 0; i < typeSize; i++) {
                        c = inputStream.read();
                        bObjectSize.add(new Byte((byte) c));
                    }
                    Byte[] bytes = bObjectSize.toArray(new Byte[bObjectSize.size()]);
                    objectSize = ByteBuffer.wrap(ArrayUtils.toPrimitive(bytes)).getInt();
                }
                if (objectSize != -1) {
                    for (int i = 0; i < objectSize; i++) {
                        c = inputStream.read();
                        bObject.add(new Byte((byte) c));
                    }

                    streaming = false;
                }
            }
            Byte[] bytes = bObject.toArray(new Byte[bObject.size()]);
            return ByteArraySerializer.deserialize(ArrayUtils.toPrimitive(bytes));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * R�cup�re une String re�u.
     * @return La String re�u.
     * @throws IOException
     */
    private String readString() throws IOException {
        boolean streaming = true;
        String strResult = "";
        int c;

        while (streaming) {
            int size = inputStream.read();

            for (int i = 0; i < size; i++) {
                c = inputStream.read();
                strResult += (char) c;
            }
            streaming = false;
        }
        return strResult;
    }

    /**
     * Permet d'envoyer un objet � tout les appareils
     * connect�s
     * @param obj
     */
    public static void sendToEveryone(Object obj) {
        RemoteMobile rm = null;
        for (ConnectedDevice connectedDevice : WaitThread.connectedDevices) {
            rm = connectedDevice.getRemoteMobile();
            if(rm.getIsAuthenticated()) {
                ProcessOutcomingThread pot = connectedDevice.getProcessOutcomingThread();
                pot.write(obj);
            }
        }
    }

    /**
     * Permet d'envoyer un objet � tout les appareils
     * connect�s EXEPTE le dernier appareil du quel on �
     * re�u des donn�es.
     *
     * Utiliser cette m�thode permet de faire transiter des
     * donn�es d'un appareil � tout les autres.
     * @param obj
     */
    private void sendToEveryoneExeptSender(Object obj) {
        RemoteMobile rm = null;
        for (ConnectedDevice connectedDevice : WaitThread.connectedDevices) {
            rm = connectedDevice.getRemoteMobile();
            if (!rm.getDevice().equals(remoteMobile.getDevice())) {
                if(rm.getIsAuthenticated()) {
                    ProcessOutcomingThread pot = connectedDevice.getProcessOutcomingThread();
                    pot.write(obj);
                }
            }
        }
    }

    /**
     * Envois des donn�es au deriner appareil du quel
     * on � re�u des donn�es.
     * @param obj
     */
    private void sendToSender(Object obj) {
        RemoteMobile rm = null;
        for (ConnectedDevice connectedDevice : WaitThread.connectedDevices) {
            rm = connectedDevice.getRemoteMobile();
            if (rm.getDevice().equals(remoteMobile.getDevice())) {
                ProcessOutcomingThread pot = connectedDevice.getProcessOutcomingThread();
                pot.write(obj);
            }
        }
    }

    /**
     * D�couvre le type de l'objet re�u et le fait
     * transiter � la bonne m�thode en conc�quence.
     * @param obj
     * @throws ParseException
     */
    private void processCommand(Object obj) throws ParseException {
        ClassType type = ClassType.valueOf(obj.getClass().getSimpleName().toUpperCase());
        switch (type) {
            case AUTHENTICATION:
                processAuthentication((Authentication) obj);
                break;
            case QUESTION:
                processQuestion((Question) obj);
                break;
            case VOTE:
                processVote((Question.Vote) obj);
                break;
        }
    }

    /**
     * Traite la r�ception d'un vote.
     * @param vote
     */
    private void processVote(Question.Vote vote) {
        String uuid = vote.getUuid();
        Question question = RemoteBluetoothServer.questions.getQuestion(uuid);
        int voteCounter;

        try {
            if (vote.isVoted) {
                System.out.println("+1 for " + uuid);
                question.vote();
            } else {
                System.out.println("-1 for " + vote.getUuid());
                question.unvote();
            }
            voteCounter = question.getVote().getVoteCounter();
            System.out.println(uuid + " has now " + voteCounter);

            // Mise � jour du compteur de vote de l'UI
            bridge.updateVoteCounter(uuid, voteCounter);
            RemoteBluetoothServer.questions.setQuestion(uuid, question);

            // Transfert le vote � tout les autres appareils connect�s
            sendToEveryoneExeptSender(vote);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Traite la r�ception d'une question
     * @param question
     * @throws ParseException
     */
    private void processQuestion(final Question question) throws ParseException {
        // Ajout de la questions au syst�me
        RemoteBluetoothServer.questions.addQuestion(question);

        // Envois de la questions � l'UI
        bridge.displayQuestion(question);
        bridge.displayNumberOfQuestionsAsked(RemoteBluetoothServer.questions.count());

        // Affichage des logs dans la console
        System.out.println("---");
        System.out.println(question.getListener().getNickname());
        System.out.println(question.getBody());
        System.out.println(question.getTime());
        System.out.println("********************");

        // Envois de la question � tout les autres appareils connect�s
        sendToEveryoneExeptSender(question);
    }

    /**
     * Traite la r�ception d'une tentative d'authentification
     * @param auth
     */
    private void processAuthentication(Authentication auth) {
        Authentication userAuth = auth;
        System.out.println("User password : " + userAuth.getPassword());

        // Validation du code
        if (pass.isValid(userAuth.getPassword()) ==
                Authentication.Status.CON_SUCCEED) {
            System.out.println("User " + remoteMobile.getDevice().getBluetoothAddress() + " connected");
            bridge.displayNumberOfConnectedListeners(WaitThread.connectedDevices.size());

            // Envois le status de connexion r�ussite
            sendToSender(Authentication.Status.CON_SUCCEED);

            // Envois des questions d�j� pos�es
            if (RemoteBluetoothServer.questions.count() > 0)
                sendToSender(RemoteBluetoothServer.questions);

            // Envois des informations de session
            if(RemoteBluetoothServer.session != null)
                sendToSender(RemoteBluetoothServer.session);

            // Mets � jour le status d'authentification de l'appareil
            for(int i = 0; i < WaitThread.connectedDevices.size(); i++) {
                ConnectedDevice connectedDevice = WaitThread.connectedDevices.get(i);
                RemoteMobile rm = connectedDevice.getRemoteMobile();
                if (rm.getDevice().equals(remoteMobile.getDevice())) {
                    connectedDevice.setIsAuthenticated(true);
                    WaitThread.connectedDevices.set(i, connectedDevice);
                }
            }
        } else {
            System.out.println("User " + remoteMobile.getDevice().getBluetoothAddress() + " failed to connect");
            // Envois le status de connexion �chou�
            sendToSender(Authentication.Status.CON_FAILED);
        }
    }
}