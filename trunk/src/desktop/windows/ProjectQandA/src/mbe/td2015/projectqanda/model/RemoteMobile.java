package mbe.td2015.projectqanda.model;

import javax.bluetooth.RemoteDevice;

public class RemoteMobile {
    /**
     * Instance de l'appareil
     */
    private RemoteDevice device;

    /**
     * Status de l'appareil relatif à l'authentification
     */
    private Boolean isAuthenticated = false;

    public RemoteMobile (RemoteDevice device) {
        this.device = device;
    }

    public Boolean getIsAuthenticated() {
        return isAuthenticated;
    }

    public void setIsAuthenticated(Boolean isAuthenticated) {
        this.isAuthenticated = isAuthenticated;
    }

    public RemoteDevice getDevice() {
        return device;
    }

    public void setDevice(RemoteDevice device) {
        this.device = device;
    }
}
