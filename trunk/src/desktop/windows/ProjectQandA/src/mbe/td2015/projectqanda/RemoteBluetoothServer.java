package mbe.td2015.projectqanda;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import mbe.td2015.projectqanda.common.model.Questions;
import mbe.td2015.projectqanda.common.model.Session;
import mbe.td2015.projectqanda.common.security.Authentication;
import mbe.td2015.projectqanda.ui.Bridge;

/**
 * Point d'entrée du programme
 */
public class RemoteBluetoothServer extends Application {
    /**
     * Instance de cette classe
     */
    private static RemoteBluetoothServer instance = null;

    /**
     * Information de la session courante
     */
    public static Session session;

    /**
     * Liste de questions
     */
    public static Questions questions;

    /**
     * Moteur du navigateur web
     */
    private static WebEngine engine;

    /**
     * Lancement de JavaFX
     */
    @Override public void start(Stage stage) {
        // Initialisation de l'instance de classe
        instance = this;

        // Initialisation de la WebView
        WebView webView = new WebView();
        webView.setContextMenuEnabled(false);

        engine = webView.getEngine();

        // Initialisation du pont de communication avec l'UI
        Bridge bridge = Bridge.getInstance();
        bridge.init(engine);

        //TODO provide a relative path
        // Chargement de la page de connexion de l'interface web
        loadPage("connection.html");

        // Mise en place de la scène (JavaFX)
        BorderPane root = new BorderPane(webView, null, null, null, null);
        Scene scene = new Scene(root);
        stage.setTitle("ProjetQ&A");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args){
        launch(args);
    }

    /**
     * Démarre la session
     */
    public static void startSession() {
        // Initialisation des attributs de la classe
        Authentication pass = new Authentication();
        questions = new Questions();

        // Lancement de la Thread d'attante de connection
        Thread waitThread = new Thread(new WaitThread(pass));
        waitThread.start();
    }

    /**
     * Charge une nouvelle page de l'interface web
     */
    public static void loadPage(String page) {
        // Chargement d'une page web (utilisation de l'instance de classe pour la charger
        // avec un chemin relatif.
        engine.load(instance.getClass().getResource("ui/www/public_html/" + page).toExternalForm());
    }
}
