package mbe.td2015.projectqanda.ui;

import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.scene.web.WebEngine;
import mbe.td2015.projectqanda.ProcessConnectionThread;
import mbe.td2015.projectqanda.RemoteBluetoothServer;
import mbe.td2015.projectqanda.common.model.Question;
import mbe.td2015.projectqanda.common.model.Session;
import mbe.td2015.projectqanda.common.model.Speaker;
import netscape.javascript.JSObject;

/**
 * Classe "Singleton" permettant de faire le pont entre le code Java de
 * l'application et le JavaScript de l'interface utilisateur.
 */
public class Bridge {
    /**
     * Instance de du Singleton
     */
    private static Bridge instance = null;

    /**
     * Instance du lien au JavaScript
     */
    private JSObject window;

    /**
     * Compteur de questions posées
     */
    private static Integer nbrQuestionsAnswered = 0;

    private Bridge() {
    }

    /**
     * Récupère l'instance du Singleton
     * @return
     */
    public static Bridge getInstance () {
        if(instance == null) {
            instance = new Bridge();
        }
        return instance;
    }

    /**
     * Initialisation du Singleton
     * @param engine
     */
    public void init(WebEngine engine) {
        if(window == null) {
            engine.getLoadWorker().stateProperty().addListener((obs, oldState, newState) -> {
                if (newState == Worker.State.SUCCEEDED) {
                    window = (JSObject) engine.executeScript("window");
                    window.setMember("java", this);
                }
            });
        }
    }

    /**
     * Permet d'ordrer les questions par votes.
     * (Appeler depuis le JavaScript)
     */
    public void btnOrderQuestionsOnClick() {
        RemoteBluetoothServer.questions.orderQuestions();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("cleanQuestions", new Object[]{});
                for (Question question : RemoteBluetoothServer.questions.getQuestions()) {
                    window.call("displayQuestion", new Object[]{question});
                }
            }
        });
    }

    /**
     * Démarre la session.
     * (Appeler depuis le JavaScript)
     * @param sessionName
     * @param speakerNickname
     */
    public void btnStartSessionOnClick(String sessionName, String speakerNickname) {
        RemoteBluetoothServer.session = new Session(sessionName, new Speaker(speakerNickname));
        RemoteBluetoothServer.startSession();
        RemoteBluetoothServer.loadPage("questions.html");
    }

    /**
     * Demande au JavaScript d'afficher le nombre d'auditeur connectés
     * @param number
     */
    public void displayNumberOfConnectedListeners (Integer number) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("displayNumberOfConnectedListeners", new Object[]{number.toString()});
            }
        });
    }

    /**
     * Demande au JavaScript d'afficher le nombre de questions posées
     * @param number
     */
    public void displayNumberOfQuestionsAsked (Integer number) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("displayNumberOfQuestionsAsked", new Object[]{number.toString()});
            }
        });
    }

    /**
     * Demande au JavaScript d'afficher le nombre de questions répondues
     * @param number
     */
    public void displayNumberOfQuestionsAnswered (Integer number) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("displayNumberOfQuestionsAnswered", new Object[]{number.toString()});
            }
        });
    }

    /**
     * Demande au JavaScript d'afficher les informations de session
     */
    public void displaySessionInfo() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("displaySessionInfo", new Object[]{RemoteBluetoothServer.session});
            }
        });
    }

    /**
     * Récupère une question répondue
     * @param uuid
     * @param isAnswered
     */
    public void questionAnswered(String uuid, boolean isAnswered) {
        Question question = RemoteBluetoothServer.questions.getQuestion(uuid);
        if (isAnswered) {
            question.answer();
            System.out.println(uuid + " has been answered !");
            nbrQuestionsAnswered++;
        } else {
            question.unanswer();
            System.out.println(uuid + " nevermind, that wasn't the right one !");
            nbrQuestionsAnswered--;
        }
        RemoteBluetoothServer.questions.setQuestion(uuid, question);
        // Mise à jour du compteur du nombre de questions répondus
        this.displayNumberOfQuestionsAnswered(nbrQuestionsAnswered);
        // Notifie tout les auditeurs que la question a été répondue
        ProcessConnectionThread.sendToEveryone(question.getAnswer());
    }

    /**
     * Demande au JavaScript d'afficher une question
     * @param question
     */
    public void displayQuestion(final Question question) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("displayQuestion", new Object[]{question});
            }
        });
    }

    /**
     * Demande au JavaScript d'afficher le code de la session
     * @param password
     */
    public void displayPassword(final String password) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("displayPassword", new Object[]{password});
            }
        });
    }

    /**
     * Demande au JavaScript de mettre à jour le compteur de vote d'une question
     * @param id
     * @param counter
     */
    public void updateVoteCounter(final String id, final int counter) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                window.call("updateVoteCounter", new Object[]{id, counter});
            }
        });
    }

}
