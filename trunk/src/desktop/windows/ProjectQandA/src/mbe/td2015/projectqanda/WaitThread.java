package mbe.td2015.projectqanda;

import mbe.td2015.projectqanda.common.security.Authentication;
import mbe.td2015.projectqanda.model.ConnectedDevice;
import mbe.td2015.projectqanda.model.RemoteMobile;
import mbe.td2015.projectqanda.ui.Bridge;

import java.io.IOException;
import java.util.ArrayList;

import javax.bluetooth.*;
import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

/**
 * Thread d'attente de la connexion d'un auditeur
 */
public class WaitThread implements Runnable{
    /**
     * Liste d'appareils connectés
     */
    public static ArrayList<ConnectedDevice> connectedDevices;

    /**
     * Instance du code de la session
     */
    private Authentication pass;

    /**
     * Instance du "pont" entre l'application et l'UI
     */
    private Bridge bridge;

	public WaitThread(Authentication pass) {
        bridge = Bridge.getInstance();
        connectedDevices = new ArrayList<ConnectedDevice>();
        this.pass = pass;
	}
	
	@Override
	public void run() {
		waitForConnection();		
	}
	
	/**
     * En attante de la connexion d'un appareil
     */
	private void waitForConnection() {
        int cptConnections = 0;
        boolean running = true;

		// Récupère une instance de l'appareil local (ordinateur)
		LocalDevice local = null;
		
		StreamConnectionNotifier notifier;
		StreamConnection connection = null;
		
		// Mise en place du serveur pour l'écoute de connexions
		try {
			local = LocalDevice .getLocalDevice();
			local.setDiscoverable(DiscoveryAgent.GIAC);

            initQuestionView();
			UUID uuid = new UUID("04c6093b00001000800000805f9b34fb", false);
			System.out.println(uuid.toString());
            System.out.println("Password : " + pass.getPassword());
            String url = "btspp://localhost:" + uuid.toString() + ";name=RemoteBluetooth";
            notifier = (StreamConnectionNotifier)Connector.open(url);
        } catch (BluetoothStateException e) {
        	System.out.println("Bluetooth is not turned on.");
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

        System.out.println("waiting for connection...");
		while(running) {
			try {
                // Attend une connexion
	            connection = notifier.acceptAndOpen();

                // Récupère l'appareil connecté
                RemoteMobile remoteMobile = new RemoteMobile(RemoteDevice.getRemoteDevice((Connection)connection));
                System.out.println("New device connected : " + remoteMobile.getDevice().getBluetoothAddress());

                // Démarre la thread qui gère la connexion et la réception de données
	            Thread processThread = new Thread(new ProcessConnectionThread(connection, pass));
	            processThread.start();

                // Démarre la thread qui gère les données en sortie
                ProcessOutcomingThread pot = new ProcessOutcomingThread(connection);
                Thread processOutcomingThread = new Thread(pot);
                processOutcomingThread.start();

                // Associe la thread de sortie à l'appareil
                connectedDevices.add(new ConnectedDevice(remoteMobile, pot));
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}

    /**
     * Envois les informations de sessions ainsi que le mot de passe à l'UI
     */
    public void initQuestionView() {
        bridge.displaySessionInfo();
        bridge.displayPassword(pass.getPassword());
    }
}
