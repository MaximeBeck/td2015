package mbe.td2015.projectqanda;

import mbe.td2015.projectqanda.common.util.ByteArraySerializer;

import javax.microedition.io.StreamConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Thread permettant de communiquer en sortie avec un appareil
 */
public class ProcessOutcomingThread implements Runnable {
    /**
     * Instance de connexion
     */
    private StreamConnection mConnection;

    /**
     * Flux de données en sortie
     */
    private OutputStream outputStream;

    public ProcessOutcomingThread(StreamConnection connection) {
        mConnection = connection;
    }

    @Override
    public void run() {
        try {
            outputStream = mConnection.openOutputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envois de données à l'appareil connecté
     * @param buffer
     */
    public void write(byte[] buffer) {
        try {
            outputStream.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(int out) {
        try {
            outputStream.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String str) {
        byte[] b = str.getBytes();
        int nbBytesInString = str.length();

        write(ProcessConnectionThread.STRING);
        write(nbBytesInString);
        write(b);
    }

    public void write(Object obj) {
        byte[] bObj = ByteArraySerializer.serialize(obj);
        int nbBytesInObject = bObj.length;
        byte[] bNb = ByteBuffer.allocate(4).putInt(nbBytesInObject).array();
        int nbBytesInNb = bNb.length;

        write(ProcessConnectionThread.OBJECT);
        write(nbBytesInNb);
        write(bNb);
        write(bObj);
    }
}
